<?php

use Lewp\Resolve;
use Lewp\RwdPicture;

return new class extends Lewp\Module
{

    const KEY_IMAGE_LIST = 'image_ids';
    const KEY_SHOW_POSITION_INDICATOR = 'show_position_indicator';
    const KEY_PREFIX_BY_PAGE_ID = 'prefix_images_by_page_id';

    private function prefix(string $image_id, array $options) {
        if (!$options[self::KEY_PREFIX_BY_PAGE_ID]) {
            return $image_id;
        }
        if (strpos($image_id, Resolve::ID_SEPARATOR) === 0) {
            return $this->getPageId().$image_id;
        } else {
            return $this->getPageId().'_'.$image_id;
        }
    }

    private function createCover(array $options)
    {
        $div = $this->createAndSetupElement(
            'div',
            '',
            [
                "class" => "cover-container"
            ]
        );

        $src_prefix = "/res/images/".$this->getModuleId()."/";
        // start image
        $image_ids = array_shift($options[self::KEY_IMAGE_LIST]);
        $start_image = new RwdPicture(
            $this->file_hierarchy,
            $this->prefix($image_ids[0], $options),
            $this->prefix($image_ids[1], $options),
            $this->getModuleId()
        );
        $img = $start_image->getPictureTag();
        $img->setAttribute("class", "cover");
        $div->appendChild($this->importNode($img, true));

        foreach ($options[self::KEY_IMAGE_LIST] as $images) {
            if (!is_array($images) || sizeof($images) !== 2) {
                trigger_error(
                    Resolve::idToUri(
                        $this->getModuleId()
                        .' requires image pairs as parameter! Please check your page file!'),
                    E_USER_WARNING
                );
                continue;
            }
            $image = new RwdPicture(
                $this->file_hierarchy,
                $this->prefix($images[0], $options),
                $this->prefix($images[1], $options),
                $this->getModuleId()
            );
            $img = $image->getPictureTag();
            $img->setAttribute("class", "cover");
            $div->appendChild($this->importNode($img, true));
        }

        return $div;
    }

    private function createPositionIndicator(array $options)
    {
        $div = $this->createAndSetupElement(
            'div',
            '',
            [
                'class' => 'position-indicator-container'
            ]
        );
        $class = 'position-indicator active';
        for ($i = 0; $i < sizeof($options[self::KEY_IMAGE_LIST]); ++$i) {
            if ($i === 1) {
                $class = 'position-indicator';
            }
            $div->appendChild(
                $this->createAndSetupElement(
                    'div',
                    '',
                    [
                        'class' => $class
                    ]
                )
            );
        }
        return $div;
    }

    public function run(array $options = []) : bool
    {
        // add default configuration
        $options += [
            self::KEY_SHOW_POSITION_INDICATOR => true,
            self::KEY_PREFIX_BY_PAGE_ID => false
        ];
        if (empty($options[self::KEY_IMAGE_LIST])) {
            trigger_error("No image has been given for the contentslider!", E_USER_WARNING);
            return false;
        }
        $this->appendChild($this->createCover($options));
        if ($options[self::KEY_SHOW_POSITION_INDICATOR]) {
            $this->appendChild($this->createPositionIndicator($options));
        }
        return true;
    }

    public function onWebsocketRequest(
        \Lewp\Websocket\Message $message
    ) : \Lewp\Websocket\Message {
    }
};
