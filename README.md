# Lewp module "eleanor"

## Description
A simple image slider that moves the first image in the container to the end every 7 seconds.

## Installation

In the terminal, go either to the global module or site module level of your hierarchy.
Then invoke the following command.
```bash
git clone git@gitlab.com:lewp/modules-contentsliders-eleanor.git modules/contentsliders/eleanor
```

## Configuration

In your page file, add the following command to add the module to your page.
```php
$this->addModule(
    "contentsliders_eleanor",
    [
    	"image_ids" => [
    		"first-image.jpg",
    		"second-image.jpg",
    		"third-image.jpg
    	],
    	"show_position_indicator" => true,
        "prefix_images_by_page_id" => true
    ]
);
```

## Available parameters

| Key | Default value | Example value | Description |
| ---- | ---- | ---- |---- |
| image_ids | | [first-image.jpg, second-image.jpg] | An array consisting of the image ids that will be shown in the slider. |
| show_position_indicator | true | false | Boolean that defines the visibility of the small position indicator. |
| prefix_images_by_page_id | false | false | If true, the given image ids are prefixed by the page id. |

## Example

![Example setup for eleanor](doc/example1.gif)
