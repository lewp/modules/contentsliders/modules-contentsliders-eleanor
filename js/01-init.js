
let intervalTime = 7000;
let coverContainer = self.querySelector('.cover-container');
// check if indicator are available
let indicatorContainer = self.querySelector('.position-indicator-container');
let activeElement = (indicatorContainer)
    ? indicatorContainer.querySelector('.active')
    : false;

// function to change front to previous front and next sibling to front
function switchFrontCover(currentFrontCover) {
    frontCover = 
        currentFrontCover.nextSibling || currentFrontCover.parentNode.childNodes[0];
    currentFrontCover.classList.remove('front');
    currentFrontCover.classList.add('previous-front');
    if (currentFrontCover.previousSibling) {
        currentFrontCover.previousSibling.classList.remove('previous-front');
    } else {
        currentFrontCover.parentNode.lastChild.classList.remove('previous-front');
    }
    frontCover.classList.add('front');
    return frontCover;
}

// function definition to move the status indicator
function updateStatusIndicator() {
    if (activeElement.nextSibling) {
        activeElement.nextSibling.after(activeElement);
    } else {
        activeElement.parentNode.childNodes[0].before(activeElement);
    }
}

// the first front cover
frontCover = coverContainer.firstChild;

if (coverContainer.children.length <= 1) {
    return;
}

if (indicatorContainer) {
    setInterval(() => {
        frontCover = switchFrontCover(frontCover);
        updateStatusIndicator();
    }, intervalTime);
} else {
    setInterval(() => {
        frontCover = switchFrontCover(frontCover);
    }, intervalTime);
}
